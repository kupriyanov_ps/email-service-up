CREATE DATABASE stuff_for_free;
CREATE USER stuff_admin WITH ENCRYPTED PASSWORD 'tstPswd';
GRANT ALL PRIVILEGES ON DATABASE stuff_for_free TO stuff_admin;
