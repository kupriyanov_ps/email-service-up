CREATE SEQUENCE price_items_id_seq
 start 1
    increment 1
    NO MAXVALUE
    CACHE 1;
ALTER TABLE price_items_id_seq
  OWNER TO stuffAdmin;

CREATE TABLE price_items
(
  id bigint NOT NULL DEFAULT nextval('price_items_id_seq'::regclass),
  vendor varchar(64),
  number varchar(64),
  search_vendor varchar(64),
  search_number varchar(64),
  description varchar(512),
  price decimal(18,2),
  count integer,
  CONSTRAINT price_items_id_p_key PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE price_items
  OWNER TO bpms;