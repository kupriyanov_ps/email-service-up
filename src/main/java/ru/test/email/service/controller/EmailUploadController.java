package ru.test.email.service.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.test.email.service.service.EmailService;

@Slf4j
@RestController
@RequestMapping(path = "/rest")
public class EmailUploadController {

    private final EmailService emailService;

    @Autowired
    public EmailUploadController(EmailService emailService) {
        this.emailService = emailService;
    }

    @GetMapping(path = "/updateDB")
    public ResponseEntity updateDatabase() {
        emailService.updateDB();
        return new ResponseEntity(HttpStatus.OK);
    }
}
