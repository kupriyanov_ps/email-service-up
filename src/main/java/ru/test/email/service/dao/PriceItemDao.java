package ru.test.email.service.dao;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.test.email.service.entity.PriceItemEntity;
import ru.test.email.service.model.PriceItemBean;
import ru.test.email.service.repository.PriceItemRepository;
import ru.test.email.service.utils.PriceItemMapper;

import java.util.List;

@Slf4j
@Service
public class PriceItemDao {
    private final PriceItemRepository priceItemRepository;
    private final PriceItemMapper priceItemMapper = new PriceItemMapper();

    @Autowired
    public PriceItemDao(PriceItemRepository priceItemRepository) {
        this.priceItemRepository = priceItemRepository;
    }

    public void savePriceItems(List<PriceItemBean> priceItemBeans) {
        priceItemBeans.forEach(priceItemBean -> {
            PriceItemEntity priceItemEntity = priceItemMapper.map(priceItemBean);
            priceItemRepository.save(priceItemEntity);
        });
    }
}
