package ru.test.email.service.entity;

import lombok.Builder;
import lombok.Getter;

import javax.persistence.*;
import java.math.BigDecimal;

import static javax.persistence.GenerationType.SEQUENCE;

@Getter
@Entity
@Builder
@Table(name = "price_items")
public class PriceItemEntity {
    @Id
    @SequenceGenerator(name = "price_items_id_seq", sequenceName = "price_items_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = SEQUENCE, generator = "price_items_id_seq")
    private Long id;
    private String vendor;
    private String number;
    private String searchVendor;
    private String searchNumber;
    private String description;
    private BigDecimal price;
    private Integer count;
}
