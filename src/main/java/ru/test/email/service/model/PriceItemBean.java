package ru.test.email.service.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PriceItemBean {
    private String vendor;
    private String number;
    private String description;
    private String price;
    private String count;
}
