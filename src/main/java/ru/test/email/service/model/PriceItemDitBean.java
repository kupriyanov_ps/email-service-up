package ru.test.email.service.model;

import com.opencsv.bean.CsvBindByName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PriceItemDitBean extends PriceItemBean {

    @CsvBindByName(column = "Бренд")
    private String vendor;
    @CsvBindByName(column = "Каталожный номер")
    private String number;
    @CsvBindByName(column = "Описание")
    private String description;
    @CsvBindByName(column = "Цена, руб.")
    private String price;
    @CsvBindByName(column = "Наличие")
    private String count;
}
