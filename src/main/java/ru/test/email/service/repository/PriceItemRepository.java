package ru.test.email.service.repository;

import org.springframework.data.repository.CrudRepository;
import ru.test.email.service.entity.PriceItemEntity;

public interface PriceItemRepository extends CrudRepository<PriceItemEntity, Long> {
}
