package ru.test.email.service.service;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.test.email.service.dao.PriceItemDao;
import ru.test.email.service.model.PriceItemBean;
import ru.test.email.service.model.PriceItemDitBean;
import ru.test.email.service.utils.EmailUtils;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.search.FlagTerm;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class EmailService {

    private final PriceItemDao priceItemDao;

    @Value("${deliver.in.time.mail.addresses}")
    private List<String> ditMailAddressList;

    @Value("${mail.protocol}")
    private String mailProtocol;
    @Value("${mail.host}")
    private String mailHost;
    @Value("${mail.port}")
    private String mailPort;
    @Value("${mail.username}")
    private String mailUserName;
    @Value("${mail.password}")
    private String mailPassword;
    @Value("${mail.folder}")
    private String mailFolder;

    @Autowired
    public EmailService(PriceItemDao priceItemDao) {
        this.priceItemDao = priceItemDao;
    }

    @Scheduled(fixedDelay = 180000) //3 minutes
    private void checkInboxMessages() {
        this.updateDB();
    }

    public void updateDB() {
        try {
            Store store = EmailUtils.getStore(mailProtocol, mailHost, mailPort);
            store.connect(mailUserName, mailPassword);

            Folder folderInbox = store.getFolder(mailFolder);
            folderInbox.open(Folder.READ_WRITE);
            Flags seen = new Flags(Flags.Flag.SEEN);
            FlagTerm unseenFlagTerm = new FlagTerm(seen, false);
            Message[] messages = folderInbox.search(unseenFlagTerm);

            for (Message message : messages) {
                Address[] fromAddresses = message.getFrom();
                String contentType = message.getContentType();
                if (contentType.contains("multipart")) {
                    try {
                        List<PriceItemBean> priceItemBeans = new ArrayList<>();
                        if (ditMailAddressList.stream()
                                .anyMatch(allowedAddress ->
                                        allowedAddress.equals(((InternetAddress) fromAddresses[0]).getAddress()))) {
                            priceItemBeans.addAll(EmailUtils.readMessage(message, PriceItemDitBean.class));
                        }

                        priceItemDao.savePriceItems(priceItemBeans);
                    } catch (Exception ex) {
                        log.error("Fail to read content: \n" + ExceptionUtils.getStackTrace(ex));
                    }
                }
            }
            folderInbox.close(false);
            store.close();
        } catch (NoSuchProviderException ex) {
            log.error("No provider for protocol: " + mailProtocol);
        } catch (MessagingException ex) {
            log.error("Could not connect to the message store");
        }
    }
}
