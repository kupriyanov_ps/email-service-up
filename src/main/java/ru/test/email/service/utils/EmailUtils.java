package ru.test.email.service.utils;

import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;

import javax.mail.*;
import javax.mail.internet.MimeBodyPart;
import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

@Slf4j
public class EmailUtils {
    public static Store getStore(String protocol, String host, String port) throws NoSuchProviderException {
        Properties properties = getServerProperties(protocol, host, port);
        Session session = Session.getDefaultInstance(properties);
        return session.getStore(protocol);
    }

    public static <T> List<T> readMessage(Message message, Class<? extends T> clazz) throws IOException, MessagingException {
        List<T> csvBeanList = new ArrayList<>();
        Multipart multiPart = (Multipart) message.getContent();
        for (int i = 0; i < multiPart.getCount(); i++) {
            MimeBodyPart part = (MimeBodyPart) multiPart.getBodyPart(i);
            if (Part.ATTACHMENT.equalsIgnoreCase(part.getDisposition())) {
                if (part.getFileName().endsWith(".csv")) {
                    Reader reader = new BufferedReader(new InputStreamReader(((InputStream) part.getContent())));
                    CsvToBean cb = new CsvToBeanBuilder(reader)
                            .withType(clazz)
                            .withSeparator(';')
                            .withIgnoreLeadingWhiteSpace(true)
                            .build();
                    Iterator<T> csvBeanIterator = cb.iterator();
                    while (csvBeanIterator.hasNext()) {
                        try {
                            csvBeanList.add(csvBeanIterator.next());
                        } catch (Exception e) {
                            log.error("Fail to read csv file: " + e.getMessage() + "\n" + ExceptionUtils.getStackTrace(e));
                        }
                    }
                    reader.close();
                }
            }
        }
        return csvBeanList;
    }

    private static Properties getServerProperties(String protocol, String host, String port) {
        Properties properties = new Properties();

        // server setting
        properties.put(String.format("mail.%s.host", protocol), host);
        properties.put(String.format("mail.%s.port", protocol), port);

        // SSL setting
        properties.setProperty(
                String.format("mail.%s.socketFactory.class", protocol),
                "javax.net.ssl.SSLSocketFactory");
        properties.setProperty(
                String.format("mail.%s.socketFactory.fallback", protocol),
                "false");
        properties.setProperty(
                String.format("mail.%s.socketFactory.port", protocol),
                String.valueOf(port));

        return properties;
    }
}
