package ru.test.email.service.utils;


import ru.test.email.service.entity.PriceItemEntity;
import ru.test.email.service.model.PriceItemBean;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.OptionalInt;

public class PriceItemMapper {

    public PriceItemEntity map(PriceItemBean priceItemBean) {

        return PriceItemEntity.builder()
                .vendor(priceItemBean.getVendor())
                .number(priceItemBean.getNumber())
                .searchNumber(normalize(priceItemBean.getNumber()))
                .searchVendor(normalize(priceItemBean.getVendor()))
                .description(priceItemBean.getDescription().length() > 512 ?
                        priceItemBean.getDescription().substring(0, 512) : priceItemBean.getDescription())
                .price(new BigDecimal(priceItemBean.getPrice().replace(',', '.'))
                        .setScale(2, BigDecimal.ROUND_CEILING))
                .count(mapCount(priceItemBean.getCount()))
                .build();
    }

    private Integer mapCount(String count) {
        if (count.contains("-")) {
            OptionalInt optionalInteger = Arrays.stream(count.split("-")).mapToInt(Integer::parseInt).max();
            return optionalInteger.isPresent() ? optionalInteger.getAsInt() : null;
        }
        if (count.contains("<") || count.contains(">")) {
            return Integer.parseInt(count.replaceAll("[^0-9]", ""));
        }
        return null;
    }

    private String normalize(String s) {
        return s.replaceAll("[^а-яА-Я0-9a-zA-Z]", "").toUpperCase();
    }
}
